import styled from 'styled-components'

export const LayoutContainer = styled.div`
  max-width: 74rem;
  margin: 0 auto;
  margin-bottom: 50px;

  display: flex;
  flex-direction: column;
`
