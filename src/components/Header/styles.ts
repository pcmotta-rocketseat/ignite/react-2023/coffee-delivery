import styled from 'styled-components'

export const HeaderContainer = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;

  padding: 1rem 0;

  img {
    width: 5.5rem;
    height: 2.5rem;
  }
`

export const ButtonsContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  gap: 12px;

  span {
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0.5rem;
    gap: 4px;

    background-color: ${(props) => props.theme['purple-300']};
    border-radius: 8px;

    color: ${(props) => props.theme['purple-700']};

    img {
      width: 22px;
      height: 22px;
    }
  }
`
