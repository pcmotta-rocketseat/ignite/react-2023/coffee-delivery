import { NavLink, useNavigate } from 'react-router-dom'
import logo from '../../assets/coffee-delivery-logo.png'
import mapPin from '../../assets/map-pin.png'
import { ShoppingCart } from './ShoppingCart'
import { ButtonsContainer, HeaderContainer } from './styles'

export function Header() {
  const navigate = useNavigate()

  return (
    <HeaderContainer>
      <NavLink to="/">
        <img src={logo} alt="" />
      </NavLink>
      <ButtonsContainer
        onClick={() => {
          navigate('/checkout')
        }}
      >
        <span>
          <img src={mapPin} alt="" />
          Porto Alegre, RS
        </span>
        <ShoppingCart />
      </ButtonsContainer>
    </HeaderContainer>
  )
}
