import { ShoppingCart as ShoppingCartIcon } from 'phosphor-react'
import { useContext } from 'react'
import { ShoppingCartContext } from '../../../contexts/ShoppingCartContext'
import { ShoppingCartContainer } from './styles'

export function ShoppingCart() {
  const { items } = useContext(ShoppingCartContext)

  return (
    <ShoppingCartContainer>
      <button>
        <ShoppingCartIcon weight="fill" />
      </button>
      {items.length > 0 && <span className="totalItems">{items.length}</span>}
    </ShoppingCartContainer>
  )
}
