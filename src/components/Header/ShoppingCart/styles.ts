import styled from 'styled-components'

export const ShoppingCartContainer = styled.div`
  display: flex;
  flex-direction: row;

  button {
    padding: 0.5rem;
    border: 0;
    border-radius: 6px;
    cursor: pointer;

    background: ${(props) => props.theme['yellow-300']};

    svg {
      fill: ${(props) => props.theme['yellow-700']};
    }
  }

  span.totalItems {
    font-size: 0.75rem;
    font-weight: 700;
    text-align: center;
    color: ${(props) => props.theme.white};

    background-color: ${(props) => props.theme['yellow-700']};
    border-radius: 1000px;

    display: flex;
    align-items: center;
    justify-content: center;

    width: 20px;
    height: 20px;

    position: relative;
    top: -8px;
    right: 8px;
  }
`
