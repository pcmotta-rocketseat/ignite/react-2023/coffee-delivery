import { NumericFormatProps, numericFormatter } from 'react-number-format'

const formatProps: NumericFormatProps = {
  decimalSeparator: ',',
  fixedDecimalScale: true,
  decimalScale: 2,
}

interface CurrencyProps {
  value: string | number
}

export function Currency(props: CurrencyProps) {
  return <>{numericFormatter(props.value.toString(), formatProps)}</>
}
