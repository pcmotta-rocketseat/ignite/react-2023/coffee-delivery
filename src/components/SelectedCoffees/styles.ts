import styled from 'styled-components'

export const SelectedCoffeesContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;

  flex: 1;

  gap: 1rem;
`

export const SelectedCoffeesTitle = styled.h1`
  font-family: 'Baloo 2', sans-serif;
  font-weight: 700;
  font-size: 1.125rem;

  color: ${(props) => props.theme['gray-800']};
`

export const Summary = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;

  width: 100%;
  gap: 1rem;

  padding: 0.5rem 2rem;
  padding-bottom: 2rem;

  background-color: ${(props) => props.theme['gray-200']};
  border-radius: 6px 44px;
`

export const SelectedItems = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  width: 100%;
`

export const Item = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;

  gap: 1.25rem;
  padding: 2rem 0;

  border-bottom: 1px solid ${(props) => props.theme['gray-400']};
  width: 100%;
`

export const ItemImgInfo = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  gap: 1rem;
`

export const ItemInfo = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;

  gap: 0.5rem;
`

export const ItemInfoButtons = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  gap: 0.5rem;
`

export const RemoveButton = styled.button`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  padding: 0.625rem;
  gap: 0.25rem;

  background-color: ${(props) => props.theme['gray-400']};
  border-radius: 6px;
  border: 0;

  font-size: 0.75rem;
  text-transform: uppercase;
  color: ${(props) => props.theme['gray-700']};

  cursor: pointer;

  svg {
    color: ${(props) => props.theme['purple-500']};
  }

  :hover {
    background-color: ${(props) => props.theme['gray-500']};
  }
`

export const ItemTotalPrice = styled.span`
  font-weight: 700;
  color: ${(props) => props.theme['gray-700']};

  ::before {
    content: 'R$';

    margin-right: 4px;
  }
`

export const TotalContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  width: 100%;
  gap: 1rem;
`

export const TotalRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  width: 100%;

  font-size: 0.875rem;
  color: ${(props) => props.theme['gray-700']};

  span {
    font-size: 1rem;
  }

  &.bold,
  &.bold > span {
    font-size: 1.25rem;
    font-weight: 700;
  }
`

export const TotalRowPrice = styled.span`
  ::before {
    content: 'R$';

    margin-right: 4px;
  }
`

export const ConfirmButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 100%;
  padding: 12px 8px;

  background-color: ${(props) => props.theme['yellow-500']};
  border-radius: 6px;
  border: 0;

  font-weight: 700;
  font-size: 0.875rem;
  font-stretch: 100;
  color: ${(props) => props.theme.white};
  text-transform: uppercase;

  cursor: pointer;

  :hover {
    background-color: ${(props) => props.theme['yellow-700']};
  }

  :disabled {
    cursor: not-allowed;
    background-color: ${(props) => props.theme['yellow-300']};
  }
`
