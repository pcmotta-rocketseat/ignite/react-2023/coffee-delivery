import { Trash } from 'phosphor-react'
import { useContext } from 'react'
import { ShoppingCartContext } from '../../contexts/ShoppingCartContext'
import { AmountSelectedItem } from '../AmountSelectedItem'
import { Currency } from '../Currency'
import {
  ConfirmButton,
  Item,
  ItemImgInfo,
  ItemInfo,
  ItemInfoButtons,
  ItemTotalPrice,
  RemoveButton,
  SelectedCoffeesContainer,
  SelectedCoffeesTitle,
  SelectedItems,
  Summary,
  TotalContainer,
  TotalRow,
  TotalRowPrice,
} from './styles'

export function SelectedCoffees() {
  const { items, changeAmount, removeItem } = useContext(ShoppingCartContext)

  const total = items.reduce((acc, value) => {
    return acc + value.item.price * value.amount
  }, 0)
  const deliveryTax = 3

  return (
    <SelectedCoffeesContainer>
      <SelectedCoffeesTitle>Cafés Selecionados</SelectedCoffeesTitle>
      <Summary>
        <SelectedItems>
          {items.map(({ item, amount }) => (
            <Item key={item.id}>
              <ItemImgInfo>
                <img src={item.img} alt="" width={64} />
                <ItemInfo>
                  <span>{item.title}</span>
                  <ItemInfoButtons>
                    <AmountSelectedItem
                      amount={amount}
                      onChange={(amount) => {
                        changeAmount(item.id, amount)
                      }}
                    />
                    <RemoveButton
                      onClick={() => {
                        removeItem(item.id)
                      }}
                    >
                      <Trash size={16} />
                      Remover
                    </RemoveButton>
                  </ItemInfoButtons>
                </ItemInfo>
              </ItemImgInfo>
              <ItemTotalPrice>
                <Currency value={item.price * amount} />
              </ItemTotalPrice>
            </Item>
          ))}
        </SelectedItems>

        <TotalContainer>
          <TotalRow>
            Total de itens
            <TotalRowPrice>
              <Currency value={total} />
            </TotalRowPrice>
          </TotalRow>
          <TotalRow>
            Entrega
            <TotalRowPrice>
              <Currency value={deliveryTax} />
            </TotalRowPrice>
          </TotalRow>
          <TotalRow className="bold">
            Total
            <TotalRowPrice>
              <Currency value={total + deliveryTax} />
            </TotalRowPrice>
          </TotalRow>
        </TotalContainer>

        <ConfirmButton type="submit" disabled={items.length === 0}>
          Confirmar Pedido
        </ConfirmButton>
      </Summary>
    </SelectedCoffeesContainer>
  )
}
