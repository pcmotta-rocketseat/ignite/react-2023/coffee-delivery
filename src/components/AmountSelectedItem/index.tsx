import { Minus, Plus } from 'phosphor-react'
import { AmountSelectedItemContainer } from './styles'

interface AmountSelectedItemProps {
  amount: number
  onChange: (amount: number) => void
}

export function AmountSelectedItem({
  amount,
  onChange,
}: AmountSelectedItemProps) {
  function decreaseAmountHandle() {
    onChange(amount - 1)
  }

  function increaseAmountHandle() {
    onChange(amount + 1)
  }

  return (
    <AmountSelectedItemContainer>
      <button
        disabled={amount === 1}
        onClick={decreaseAmountHandle}
        type="button"
      >
        <Minus weight="fill" size={14} />
      </button>
      {amount}
      <button onClick={increaseAmountHandle} type="button">
        <Plus weight="fill" size={14} />
      </button>
    </AmountSelectedItemContainer>
  )
}
