import styled from 'styled-components'

export const AmountSelectedItemContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  color: ${(props) => props.theme['gray-900']};
  padding: 0.5rem;

  gap: 0.5rem;

  background-color: ${(props) => props.theme['gray-400']};
  border-radius: 6px;

  button {
    border: 0;
    background-color: ${(props) => props.theme['gray-400']};
    cursor: pointer;

    svg {
      fill: ${(props) => props.theme['purple-500']};
    }

    :disabled {
      cursor: not-allowed;
    }

    :disabled svg {
      fill: ${(props) => props.theme['gray-200']};
    }
  }
`
