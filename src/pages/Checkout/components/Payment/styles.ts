import styled from 'styled-components'

export const PaymentContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;

  width: 100%;

  gap: 2rem;
  padding: 2.5rem;

  background-color: ${(props) => props.theme['gray-200']};
  border-radius: 6px;

  svg {
    color: ${(props) => props.theme['purple-500']};
  }
`

export const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  gap: 0.75rem;

  width: 100%;
`

export const PaymentMethod = styled.button`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;

  flex: 1;

  gap: 0.75rem;
  padding: 1rem;

  background-color: ${(props) => props.theme['gray-400']};
  border-radius: 6px;
  border: 1px solid transparent;

  font-size: 0.75rem;
  line-height: 160%;
  color: ${(props) => props.theme['gray-700']};
  text-transform: uppercase;

  cursor: pointer;

  svg {
    color: ${(props) => props.theme['purple-500']};
  }

  :hover {
    background-color: ${(props) => props.theme['gray-500']};
  }

  &.payment-active {
    background-color: ${(props) => props.theme['purple-300']};
    border: 1px solid ${(props) => props.theme['purple-500']};
  }
`

export const ErrorMessage = styled.div`
  color: red;
  font-size: 0.75rem;

  height: 10px;
`
