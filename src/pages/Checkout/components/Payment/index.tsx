import { Bank, CreditCard, CurrencyDollar, Money } from 'phosphor-react'
import { useState } from 'react'
import { useFormContext } from 'react-hook-form'

import { SectionTitle } from '../SectionTitle'
import {
  ButtonsContainer,
  ErrorMessage,
  PaymentContainer,
  PaymentMethod,
} from './styles'

// eslint-disable-next-line no-unused-vars
export enum PAYMENT_METHOD {
  // eslint-disable-next-line no-unused-vars
  CREDITO = 'CREDITO',
  // eslint-disable-next-line no-unused-vars
  DEBITO = 'DEBITO',
  // eslint-disable-next-line no-unused-vars
  DINHEIRO = 'DINHEIRO',
}

export function Payment() {
  const { register, setValue, formState } = useFormContext()
  const [paymentMethodActive, setPaymentMethodActive] =
    useState<PAYMENT_METHOD | null>(null)

  function paymentMethodSelected(id: PAYMENT_METHOD) {
    setPaymentMethodActive((state) => {
      if (state === id) {
        setValue('paymentMethod', '', { shouldValidate: true })
        return null
      }

      setValue('paymentMethod', id, { shouldValidate: true })
      return id
    })
  }

  const activeClass = 'payment-active'

  return (
    <PaymentContainer>
      <SectionTitle
        title="Pagamento"
        subtitle="O pagamento é feito na entrega. Escolha a forma que deseja pagar"
        icon={<CurrencyDollar />}
      />

      <ButtonsContainer>
        <input
          type="hidden"
          id="paymentMethod"
          {...register('paymentMethod')}
        />

        <PaymentMethod
          type="button"
          onClick={() => paymentMethodSelected(PAYMENT_METHOD.CREDITO)}
          className={
            paymentMethodActive === PAYMENT_METHOD.CREDITO ? activeClass : ''
          }
        >
          <CreditCard size={16} />
          Cartão de Crédito
        </PaymentMethod>

        <PaymentMethod
          type="button"
          onClick={() => paymentMethodSelected(PAYMENT_METHOD.DEBITO)}
          className={
            paymentMethodActive === PAYMENT_METHOD.DEBITO ? activeClass : ''
          }
        >
          <Bank size={16} />
          Cartão de Débito
        </PaymentMethod>

        <PaymentMethod
          type="button"
          onClick={() => paymentMethodSelected(PAYMENT_METHOD.DINHEIRO)}
          className={
            paymentMethodActive === PAYMENT_METHOD.DINHEIRO ? activeClass : ''
          }
        >
          <Money size={16} />
          Dinheiro
        </PaymentMethod>
      </ButtonsContainer>

      <ErrorMessage>
        {formState.errors.paymentMethod?.message?.toString()}
      </ErrorMessage>
    </PaymentContainer>
  )
}
