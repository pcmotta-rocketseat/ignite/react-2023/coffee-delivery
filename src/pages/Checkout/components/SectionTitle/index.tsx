import { ReactNode } from 'react'
import { Subtitle, TextContainer, Title, TitleContainer } from './styles'

interface SectionTitleProps {
  title: string
  subtitle: string
  icon: ReactNode
}

export function SectionTitle(props: SectionTitleProps) {
  return (
    <TitleContainer>
      {props.icon}
      <TextContainer>
        <Title>{props.title}</Title>
        <Subtitle>{props.subtitle}</Subtitle>
      </TextContainer>
    </TitleContainer>
  )
}
