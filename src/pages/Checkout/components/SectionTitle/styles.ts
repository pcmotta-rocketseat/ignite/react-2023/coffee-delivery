import styled from 'styled-components'

export const TitleContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;

  gap: 0.5rem;
`

export const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;

  gap: 0.25rem;
`

export const Title = styled.span`
  color: ${(props) => props.theme['gray-800']};
`

export const Subtitle = styled.span`
  font-size: 0.875rem;
  color: ${(props) => props.theme['gray-800']};
`
