import { InputHTMLAttributes } from 'react'
import { useFormContext } from 'react-hook-form'
import { InputContainer, Input as HtmlInput, InputError } from './styles'

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  id: string
  size?: 100 | 60 | 50 | 10
  optional?: boolean
}

export function Input(props: InputProps) {
  const { register, formState } = useFormContext()

  let className = props.size ? `w${props.size}` : ''
  const errorMessage = formState.errors[props.id]?.message || ''

  if (props.optional) {
    className = `${className} optional`
  }

  return (
    <InputContainer className={className}>
      <HtmlInput
        {...props}
        {...register(props.id, {
          valueAsNumber: props.type && props.type === 'number',
        })}
        className={formState.errors[props.id] ? 'is-invalid' : ''}
      />
      <InputError>{`${errorMessage}`}</InputError>
    </InputContainer>
  )
}
