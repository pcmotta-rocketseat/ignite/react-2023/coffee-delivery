import styled from 'styled-components'

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;

  width: 40%;
  position: relative;

  &.w100 {
    width: 100%;
  }

  &.w60 {
    width: calc(60% - 0.75rem);
  }

  &.w50 {
    width: calc(50% - 0.75rem);
  }

  &.w10 {
    width: calc(10% - 0.75rem);
  }

  &.optional {
    :focus-within {
      ::after {
        content: '' !important;
      }
    }

    :has(input:placeholder-shown) {
      ::after {
        content: 'Opcional';

        position: absolute;
        right: 12px;
        top: 15px;

        font-style: italic;
        font-size: 0.75rem;
        color: ${(props) => props.theme['gray-600']};
      }
    }
  }
`

export const Input = styled.input`
  padding: 0.75rem;

  background: ${(props) => props.theme['gray-300']};
  border: 1px solid ${(props) => props.theme['gray-400']};
  border-radius: 4px;

  font-size: 0.875rem;
  color: ${(props) => props.theme['gray-700']};

  width: 100%;

  :focus {
    border: 1px solid ${(props) => props.theme['yellow-700']};
  }

  &.is-invalid {
    border: 1px solid red;
  }
`

export const InputError = styled.div`
  font-size: 0.75rem;
  color: red;

  height: 10px;
`
