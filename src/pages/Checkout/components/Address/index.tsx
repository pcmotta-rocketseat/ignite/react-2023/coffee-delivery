import { MapPinLine } from 'phosphor-react'

import { Input } from '../Input'
import { SectionTitle } from '../SectionTitle'
import { AddressContainer, FormContainer, Row } from './styles'
import { useFormContext } from 'react-hook-form'

const formOptions = { shouldValidate: true }

export function Address() {
  const { setValue } = useFormContext()

  function onCepBlur(event: any) {
    const cep = event.target.value

    fetch(`https://viacep.com.br/ws/${cep.replace('.', '')}/json/`)
      .then((response) => response.json())
      .then((response) => {
        setValue('cep', response.cep, formOptions)
        setValue('rua', response.logradouro, formOptions)
        setValue('bairro', response.bairro, formOptions)
        setValue('cidade', response.localidade, formOptions)
        setValue('uf', response.uf, formOptions)
      })
  }

  return (
    <AddressContainer>
      <SectionTitle
        title="Endereço de Entrega"
        subtitle="Informe o endereço onde deseja receber seu pedido"
        icon={<MapPinLine />}
      />

      <FormContainer>
        <Input id="cep" placeholder="CEP" onBlurCapture={onCepBlur} />
        <Input id="rua" placeholder="Rua" size={100} />
        <Row>
          <Input id="numero" placeholder="Número" type="number" />
          <Input
            id="complemento"
            placeholder="Complemento"
            size={60}
            optional
          />
        </Row>
        <Row>
          <Input id="bairro" placeholder="Bairro" />
          <Input id="cidade" placeholder="Cidade" size={50} />
          <Input id="uf" placeholder="UF" size={10} />
        </Row>
      </FormContainer>
    </AddressContainer>
  )
}
