import styled from 'styled-components'

export const AddressContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;

  gap: 2rem;

  width: 100%;
  padding: 2rem;

  background-color: ${(props) => props.theme['gray-200']};

  svg {
    color: ${(props) => props.theme['yellow-700']};
  }
`

export const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;

  gap: 1rem;

  width: 100%;
`

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;

  gap: 0.75rem;

  width: 100%;
`
