import { useForm, FormProvider } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import * as zod from 'zod'
import { zodResolver } from '@hookform/resolvers/zod'

import { SelectedCoffees } from '../../components/SelectedCoffees'
import { Address } from './components/Address'
import { Payment } from './components/Payment'
import { CheckoutContainer, PaymentAndAddressContainer } from './styles'
import { useContext } from 'react'
import { ShoppingCartContext } from '../../contexts/ShoppingCartContext'

export const checkoutFormValidationSchema = zod.object({
  cep: zod
    .string()
    .min(9, 'Permitido somente 9 caracteres')
    .max(9, 'Permitido somente 9 caracteres'),
  rua: zod.string().min(5, 'Deve haver no mínimo 5 caracteres'),
  numero: zod.number().min(1, 'Não pode ser menor do que 1'),
  complemento: zod.string(),
  bairro: zod.string().min(3, 'Deve haver no mínimo 3 caracteres'),
  cidade: zod.string().min(5, 'Deve haver no mínimo 5 caracteres'),
  uf: zod
    .string()
    .min(2, 'Permitido somente 2 caracteres')
    .max(2, 'Permitido somente 2 caracteres'),
  paymentMethod: zod.string().nonempty('O método de pagamento é obrigatório'),
})

export const checkoutFormInitialValues = {
  resolver: zodResolver(checkoutFormValidationSchema),
  defaultValues: {
    cep: '',
    rua: '',
    numero: 0,
    complemento: '',
    bairro: '',
    cidade: '',
    uf: '',
    paymentMethod: '',
  },
}

export type CheckoutFormData = zod.infer<typeof checkoutFormValidationSchema>

export function Checkout() {
  const navigate = useNavigate()
  const form = useForm<CheckoutFormData>(checkoutFormInitialValues)
  const { addCheckoutFormData } = useContext(ShoppingCartContext)

  function handleConfirmCheckout(data: CheckoutFormData) {
    addCheckoutFormData(data)
    form.reset()
    navigate('/confirm')
  }

  return (
    <form onSubmit={form.handleSubmit(handleConfirmCheckout)} action="">
      <CheckoutContainer>
        <PaymentAndAddressContainer>
          <h1>Complete seu pedido</h1>
          <FormProvider {...form}>
            <Address />
            <Payment />
          </FormProvider>
        </PaymentAndAddressContainer>
        <SelectedCoffees />
      </CheckoutContainer>
    </form>
  )
}
