import styled from 'styled-components'
import homeBackground from '../../../assets/home-background.png'

export const BannerContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  gap: 7rem;

  padding: 6rem 0;

  background: url(${homeBackground});
`

export const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;

  gap: 6rem;
`

export const TituloContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;

  gap: 1rem;

  h1 {
    font-family: 'Baloo 2', sans-serif;
    font-weight: 800;
    font-size: 3rem;
    line-height: 130%;

    color: ${(props) => props.theme['gray-900']};
  }

  h2 {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
    font-style: normal;
    font-size: 1.25rem;
    line-height: 130%;

    color: ${(props) => props.theme['gray-900']};
    font-stretch: 100;
  }
`
