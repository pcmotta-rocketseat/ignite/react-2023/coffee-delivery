import { Highlights } from '../Highlights'
import { BannerContainer, TextContainer, TituloContainer } from './styles'

import capaHome from '../../../assets/capa-home.png'

export function Banner() {
  return (
    <BannerContainer>
      <TextContainer>
        <TituloContainer>
          <h1>Encontre o café perfeito para qualquer hora do dia</h1>
          <h2>
            Com o Coffee Delivery você recebe seu café onde estiver, a qualquer
            hora
          </h2>
        </TituloContainer>
        <Highlights />
      </TextContainer>
      <div>
        <img src={capaHome} alt="" />
      </div>
    </BannerContainer>
  )
}
