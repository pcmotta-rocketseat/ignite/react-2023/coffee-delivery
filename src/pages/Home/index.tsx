import { HomeContainer } from './styles'
import { Banner } from './Banner'
import { Menu } from './Menu'

export function Home() {
  return (
    <HomeContainer>
      <Banner />
      <Menu />
    </HomeContainer>
  )
}
