import { ShoppingCart } from 'phosphor-react'
import {
  GrayHighlight,
  HighlightColumnContainer,
  HighlightsContainer,
  LightYellowHighlight,
  PurpleHighlight,
  YellowHighlight,
} from './styles'

export function Highlights() {
  return (
    <HighlightsContainer>
      <HighlightColumnContainer>
        <YellowHighlight>
          <span>
            <ShoppingCart weight="fill" />
          </span>
          Compra simples e segura
        </YellowHighlight>
        <LightYellowHighlight>
          <span>
            <ShoppingCart weight="fill" />
          </span>
          Entrega rápida e rastreada
        </LightYellowHighlight>
      </HighlightColumnContainer>
      <HighlightColumnContainer>
        <GrayHighlight>
          <span>
            <ShoppingCart weight="fill" />
          </span>
          Embalagem mantém o café intacto
        </GrayHighlight>
        <PurpleHighlight>
          <span>
            <ShoppingCart weight="fill" />
          </span>
          O café chega fresquinho até você
        </PurpleHighlight>
      </HighlightColumnContainer>
    </HighlightsContainer>
  )
}
