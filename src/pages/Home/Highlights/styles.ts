import styled from 'styled-components'

export const HighlightsContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  gap: 2.5rem;
`

export const HighlightColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;

  gap: 1.25rem;
`

const Highlight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  gap: 8px;

  span {
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 8px;

    border-radius: 45px;
    background-color: ${(props) => props.theme['yellow-700']};

    svg {
      fill: ${(props) => props.theme.white};
    }
  }
`

export const YellowHighlight = styled(Highlight)`
  span {
    background-color: ${(props) => props.theme['yellow-700']};
  }
`

export const LightYellowHighlight = styled(Highlight)`
  span {
    background-color: ${(props) => props.theme['yellow-500']};
  }
`

export const GrayHighlight = styled(Highlight)`
  span {
    background-color: ${(props) => props.theme['gray-900']};
  }
`

export const PurpleHighlight = styled(Highlight)`
  span {
    background-color: ${(props) => props.theme['purple-500']};
  }
`
