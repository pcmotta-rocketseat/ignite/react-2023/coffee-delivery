import { MenuItem, MenuItemType } from './MenuItem'
import { MenuContainer, MenuItemsContainer } from './styles'

import tradicional from '../../../assets/coffees/tradicional.png'
import americano from '../../../assets/coffees/americano.png'
import cremoso from '../../../assets/coffees/cremoso.png'
import gelado from '../../../assets/coffees/gelado.png'
import leite from '../../../assets/coffees/leite.png'
import latte from '../../../assets/coffees/latte.png'
import capuccino from '../../../assets/coffees/capuccino.png'
import macchiato from '../../../assets/coffees/macchiato.png'
import mocaccino from '../../../assets/coffees/mocaccino.png'
import quente from '../../../assets/coffees/quente.png'
import cubano from '../../../assets/coffees/cubano.png'
import havaiano from '../../../assets/coffees/havaiano.png'
import arabe from '../../../assets/coffees/arabe.png'
import irlandes from '../../../assets/coffees/irlandes.png'

import { CoffeeTypeEnum } from './MenuItem/CoffeeTypeEnum'

const items: MenuItemType[] = [
  buildMenuItemType(
    'a',
    tradicional,
    'Expresso Tradicional',
    'O tradicional café feito com água quente e grãos',
    [CoffeeTypeEnum.TRADICIONAL],
  ),
  buildMenuItemType(
    'b',
    americano,
    'Expresso Americano',
    'Expresso diluído, menos intenso que o tradicional',
    [CoffeeTypeEnum.TRADICIONAL],
  ),
  buildMenuItemType(
    'c',
    cremoso,
    'Expresso Cremoso',
    'Café expresso tradicional com espuma cremosa',
    [CoffeeTypeEnum.TRADICIONAL],
  ),
  buildMenuItemType(
    'd',
    gelado,
    'Expresso Gelado',
    'Bebida preparada com café expresso e cubos de gelo',
    [CoffeeTypeEnum.TRADICIONAL, CoffeeTypeEnum.GELADO],
  ),
  buildMenuItemType(
    'e',
    leite,
    'Café com Leite',
    'Meio a meio de expresso tradicional com leite vaporizado',
    [CoffeeTypeEnum.TRADICIONAL, CoffeeTypeEnum.LEITE],
  ),
  buildMenuItemType(
    'f',
    latte,
    'Latte',
    'Uma dose de café expresso com o dobro de leite e espuma cremosa',
    [CoffeeTypeEnum.TRADICIONAL, CoffeeTypeEnum.LEITE],
  ),
  buildMenuItemType(
    'g',
    capuccino,
    'Capuccino',
    'Bebida com canela feita de doses iguais de café, leite e espuma',
    [CoffeeTypeEnum.TRADICIONAL, CoffeeTypeEnum.LEITE],
  ),
  buildMenuItemType(
    'h',
    macchiato,
    'Macchiato',
    'Café expresso misturado com um pouco de leite quente e espuma',
    [CoffeeTypeEnum.TRADICIONAL, CoffeeTypeEnum.LEITE],
  ),
  buildMenuItemType(
    'i',
    mocaccino,
    'Mocaccino',
    'Café expresso com calda de chocolate, pouco leite e espuma',
    [CoffeeTypeEnum.TRADICIONAL, CoffeeTypeEnum.LEITE],
  ),
  buildMenuItemType(
    'j',
    quente,
    'Chocolate Quente',
    'Bebida feita com chocolate dissolvido no leite quente e café',
    [CoffeeTypeEnum.ESPECIAL, CoffeeTypeEnum.LEITE],
  ),
  buildMenuItemType(
    'k',
    cubano,
    'Cubano',
    'Drink gelado de café expresso com rum, creme de leite e hortelã',
    [CoffeeTypeEnum.ESPECIAL, CoffeeTypeEnum.ALCOOLICO, CoffeeTypeEnum.GELADO],
  ),
  buildMenuItemType(
    'l',
    havaiano,
    'Havaiano',
    'Bebida adocicada preparada com café e leite de coco',
    [CoffeeTypeEnum.ESPECIAL],
  ),
  buildMenuItemType(
    'm',
    arabe,
    'Árabe',
    'Bebida preparada com grãos de café árabe e especiarias',
    [CoffeeTypeEnum.ESPECIAL],
  ),
  buildMenuItemType(
    'n',
    irlandes,
    'Irlandês',
    'Bebida a base de café, uísque irlandês, açúcar e chantilly',
    [CoffeeTypeEnum.ESPECIAL, CoffeeTypeEnum.ALCOOLICO],
  ),
]

function buildMenuItemType(
  id: string,
  img: string,
  title: string,
  subtitle: string,
  types: CoffeeTypeEnum[],
): MenuItemType {
  return {
    id,
    img,
    title,
    subtitle,
    price: 9.9,
    types,
  }
}

export function Menu() {
  return (
    <MenuContainer>
      <h2>Nossos cafés</h2>
      <MenuItemsContainer>
        {items.map((item) => (
          <MenuItem key={item.id} item={item} />
        ))}
      </MenuItemsContainer>
    </MenuContainer>
  )
}
