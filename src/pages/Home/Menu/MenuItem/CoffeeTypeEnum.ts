/* eslint-disable no-unused-vars */
export enum CoffeeTypeEnum {
  TRADICIONAL = 'Tradicional',
  GELADO = 'Gelado',
  LEITE = 'Com Leite',
  ESPECIAL = 'Especial',
  ALCOOLICO = 'Alcoólico',
}
