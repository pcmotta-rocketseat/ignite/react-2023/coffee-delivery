import { ShoppingCart } from 'phosphor-react'
import { useContext, useState } from 'react'
import { toast } from 'react-toastify'
import { AmountSelectedItem } from '../../../../components/AmountSelectedItem'
import { Currency } from '../../../../components/Currency'
import { ShoppingCartContext } from '../../../../contexts/ShoppingCartContext'

import { CoffeeTypeEnum } from './CoffeeTypeEnum'
import {
  AddCoffee,
  AmountItemsContainer,
  CoffeePrice,
  CoffeeType,
  CoffeeTypeContainer,
  CoffePriceContainer,
  MenuItemContainer,
  MenuItemSubtitle,
} from './styles'

export interface MenuItemType {
  id: string
  title: string
  subtitle: string
  img: string
  price: number
  types: CoffeeTypeEnum[]
}

interface MenuItemProps {
  item: MenuItemType
}

export function MenuItem({ item }: MenuItemProps) {
  const { addNewItem } = useContext(ShoppingCartContext)
  const [amount, setAmount] = useState(1)

  function addItem() {
    addNewItem({
      item,
      amount,
    })

    toast(`Café ${item.title} adicionado`, {
      type: toast.TYPE.SUCCESS,
    })
  }

  return (
    <MenuItemContainer>
      <img src={item.img} width={120} alt="" />
      <CoffeeTypeContainer>
        {item.types.map((type) => (
          <CoffeeType key={type}>{type}</CoffeeType>
        ))}
      </CoffeeTypeContainer>
      <h1>{item.title}</h1>
      <MenuItemSubtitle>{item.subtitle}</MenuItemSubtitle>
      <CoffePriceContainer>
        <CoffeePrice>
          <Currency value={item.price} />
        </CoffeePrice>
        <AmountItemsContainer>
          <AmountSelectedItem amount={amount} onChange={setAmount} />
          <AddCoffee onClick={addItem}>
            <ShoppingCart weight="fill" fill="#FFF" size={20} />
          </AddCoffee>
        </AmountItemsContainer>
      </CoffePriceContainer>
    </MenuItemContainer>
  )
}
