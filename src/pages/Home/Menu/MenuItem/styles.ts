import styled from 'styled-components'

export const MenuItemContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  background: ${(props) => props.theme['gray-200']};
  border-radius: 6px 36px;

  gap: 1rem;

  width: 16rem;
  margin-top: 12px;

  img {
    margin-top: -2rem;
  }

  h1 {
    font-family: 'Baloo 2', sans-serif;
    font-weight: 700;
    font-size: 1.25rem;
    line-height: 130%;

    color: ${(props) => props.theme['gray-800']};
  }
`

export const MenuItemSubtitle = styled.span`
  font-weight: 400;
  font-size: 0.875rem;
  line-height: 130%;
  color: ${(props) => props.theme['gray-600']};

  margin-top: -0.75rem;
  text-align: center;
  width: 85%;
`

export const CoffeeTypeContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  gap: 0.5rem;
`

export const CoffeeType = styled.span`
  font-size: 0.625rem;
  font-weight: 700;
  padding: 4px 8px;

  background: ${(props) => props.theme['yellow-300']};
  border-radius: 100px;

  text-transform: uppercase;
  color: ${(props) => props.theme['yellow-700']};
  line-height: 130%;
`

export const CoffePriceContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;

  gap: 2.5rem;

  padding: 1.25rem 0;
`

export const CoffeePrice = styled.span`
  font-family: 'Baloo 2', sans-serif;
  font-weight: 800;
  font-size: 1.5rem;
  text-align: right;
  color: ${(props) => props.theme['gray-700']};

  ::before {
    content: 'R$';
    font-family: 'Roboto', sans-serif;
    font-size: 0.875rem;
    font-weight: 400;

    margin-right: 4px;
  }
`

export const AmountItemsContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  gap: 0.5rem;
`

export const AddCoffee = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 8px;

  background-color: ${(props) => props.theme['purple-700']};
  border-radius: 6px;
  border: 0;

  cursor: pointer;

  :hover {
    background-color: ${(props) => props.theme['purple-500']};
  }
`
