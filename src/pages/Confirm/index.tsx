import { Clock, CurrencyDollar, MapPin } from 'phosphor-react'
import { useContext } from 'react'
import { ShoppingCartContext } from '../../contexts/ShoppingCartContext'
import { PAYMENT_METHOD } from '../Checkout/components/Payment'
import {
  ConfirmContainer,
  InfoContainer,
  OrderContainer,
  OrderItem,
  TextContainer,
} from './styles'

import confirmBackground from '../../assets/confirm-background.png'

export function Confirm() {
  const { checkoutFormData } = useContext(ShoppingCartContext)
  const { rua, numero, complemento, bairro, cidade, uf, paymentMethod } =
    checkoutFormData

  const address = `${rua}, ${numero}${complemento ? ' - ' + complemento : ''}`

  const paymentMethodName = (paymentMethod: string) => {
    switch (paymentMethod) {
      case PAYMENT_METHOD.CREDITO:
        return 'Cartão de Crédito'
      case PAYMENT_METHOD.DEBITO:
        return 'Cartão de Débito'
      case PAYMENT_METHOD.DINHEIRO:
        return 'Dinheiro'
      default:
        return ''
    }
  }

  return (
    <ConfirmContainer>
      <InfoContainer>
        <TextContainer>
          <h1>Uhu! Pedido confirmado</h1>
          <h2>Agora é só aguardar que logo o café chegará até você</h2>
        </TextContainer>
        <OrderContainer>
          <OrderItem>
            <span>
              <MapPin size={16} weight="fill" />
            </span>
            <div>
              Entrega em <b>{address}</b> <br />
              {bairro} - {cidade}, {uf}
            </div>
          </OrderItem>
          <OrderItem>
            <span>
              <Clock size={16} weight="fill" />
            </span>
            <div>
              Previsão de entrega <br />
              <b>20 min - 30 min</b>
            </div>
          </OrderItem>

          <OrderItem>
            <span>
              <CurrencyDollar size={16} weight="fill" />
            </span>
            <div>
              Pagamento na entrega <br />
              <b>{paymentMethodName(paymentMethod)}</b>
            </div>
          </OrderItem>
        </OrderContainer>
      </InfoContainer>
      <img src={confirmBackground} alt="" />
    </ConfirmContainer>
  )
}
