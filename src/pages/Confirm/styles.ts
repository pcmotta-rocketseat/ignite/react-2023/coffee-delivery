import styled from 'styled-components'

export const ConfirmContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: space-between;

  gap: 5rem;
  margin-top: 40px;
`

export const InfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;
  flex: 2;

  gap: 4rem;
`

export const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;

  > h1 {
    font-family: 'Baloo 2';
    font-weight: 800;
    font-size: 2rem;

    color: ${(props) => props.theme['yellow-700']} !important;
  }

  > h2 {
    font-size: 1.25rem;
    font-weight: 400;
    color: ${(props) => props.theme['gray-800']};
  }
`

export const OrderContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;

  padding: 2.5rem;
  gap: 2rem;
  width: 100%;

  border: 1px solid transparent;
  border-radius: 6px 36px;
  background: linear-gradient(#fff, #fff) padding-box,
    linear-gradient(to right, #dbac2c, #8047f8) border-box;
`

export const OrderItem = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  gap: 1rem;

  > span {
    display: flex;
    align-items: center;
    justify-content: center;

    border: 0;
    border-radius: 999px;

    padding: 8px;

    svg {
      fill: ${(props) => props.theme.white};
    }
  }

  :nth-of-type(1) {
    > span {
      background-color: ${(props) => props.theme['purple-500']};
    }
  }

  :nth-of-type(2) {
    > span {
      background-color: ${(props) => props.theme['yellow-500']};
    }
  }

  :nth-of-type(3) {
    > span {
      background-color: ${(props) => props.theme['yellow-700']};
    }
  }
`
