import produce from 'immer'
import { CheckoutFormData } from '../../pages/Checkout'
import { MenuItemType } from '../../pages/Home/Menu/MenuItem'
import { ActionTypes } from './actions'

export interface ShoppingCartItem {
  item: MenuItemType
  amount: number
}

interface ShoppingCartState {
  items: ShoppingCartItem[]
  checkoutFormData: CheckoutFormData
}

export interface ShoppingCartAction {
  type: string
  payload?: any
}

export function shoppingCartReducer(
  state: ShoppingCartState,
  action: ShoppingCartAction,
) {
  switch (action.type) {
    case ActionTypes.ADD_NEW_ITEM:
      return produce(state, ({ items }) => {
        const item = items.find(
          ({ item }) => item.id === action.payload.item.item.id,
        )

        if (!item) {
          items.push(action.payload.item)
        } else {
          item.amount = item.amount + action.payload.item.amount
        }
      })
    case ActionTypes.CHANGE_AMOUNT:
      return produce(state, ({ items }) => {
        const item = items.find(({ item }) => item.id === action.payload.id)

        if (item) {
          item.amount = action.payload.amount
        }
      })
    case ActionTypes.REMOVE_ITEM:
      return produce(state, (draft) => {
        draft.items = draft.items.filter(
          ({ item }) => item.id !== action.payload.id,
        )
      })
    case ActionTypes.ADD_CHECKOUT_FORM_DATA:
      return produce(state, (draft) => {
        draft.items = []
        draft.checkoutFormData = action.payload.data
      })
    default:
      return state
  }
}
