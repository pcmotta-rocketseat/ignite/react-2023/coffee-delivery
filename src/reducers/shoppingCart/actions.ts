/* eslint-disable no-unused-vars */
import { CheckoutFormData } from '../../pages/Checkout'
import { ShoppingCartAction, ShoppingCartItem } from './reducer'

export enum ActionTypes {
  ADD_NEW_ITEM = 'ADD_NEW_ITEM',
  CHANGE_AMOUNT = 'CHANGE_AMOUNT',
  REMOVE_ITEM = 'REMOVE_ITEM',
  ADD_CHECKOUT_FORM_DATA = 'ADD_CHECKOUT_FORM_DATA',
}

export function addNewItemAction(item: ShoppingCartItem): ShoppingCartAction {
  return {
    type: ActionTypes.ADD_NEW_ITEM,
    payload: {
      item,
    },
  }
}

export function changeAmountAction(id: string, amount: number) {
  return {
    type: ActionTypes.CHANGE_AMOUNT,
    payload: {
      id,
      amount,
    },
  }
}

export function removeItemAction(id: string) {
  return {
    type: ActionTypes.REMOVE_ITEM,
    payload: {
      id,
    },
  }
}

export function addCheckoutFormDataAction(data: CheckoutFormData) {
  return {
    type: ActionTypes.ADD_CHECKOUT_FORM_DATA,
    payload: {
      data,
    },
  }
}
