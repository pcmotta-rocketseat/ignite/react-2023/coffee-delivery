import { createContext, ReactNode, useEffect, useReducer } from 'react'
import { CheckoutFormData, checkoutFormInitialValues } from '../pages/Checkout'
import {
  addCheckoutFormDataAction,
  addNewItemAction,
  changeAmountAction,
  removeItemAction,
} from '../reducers/shoppingCart/actions'
import {
  ShoppingCartItem,
  shoppingCartReducer,
} from '../reducers/shoppingCart/reducer'

interface ShoppingCartContextType {
  items: ShoppingCartItem[]
  checkoutFormData: CheckoutFormData
  addNewItem: (item: ShoppingCartItem) => void
  changeAmount: (id: string, amount: number) => void
  removeItem: (id: string) => void
  addCheckoutFormData: (data: CheckoutFormData) => void
}

export const ShoppingCartContext = createContext({} as ShoppingCartContextType)

interface ShoppingCartProviderProps {
  children: ReactNode
}

export function ShoppingCartProvider({ children }: ShoppingCartProviderProps) {
  const [shoppingCartState, dispatch] = useReducer(
    shoppingCartReducer,
    {
      items: [],
      checkoutFormData: checkoutFormInitialValues.defaultValues,
    },
    (initialState) => {
      const storedStateAsJSON = localStorage.getItem(
        '@coffee-delivery:shopping-cart-state-1.0.0',
      )

      if (storedStateAsJSON) {
        return JSON.parse(storedStateAsJSON)
      }

      return initialState
    },
  )

  useEffect(() => {
    const stateJSON = JSON.stringify(shoppingCartState)
    localStorage.setItem(
      '@coffee-delivery:shopping-cart-state-1.0.0',
      stateJSON,
    )
  }, [shoppingCartState])

  const { items, checkoutFormData } = shoppingCartState

  function addNewItem(item: ShoppingCartItem) {
    dispatch(addNewItemAction(item))
  }

  function changeAmount(id: string, amount: number) {
    dispatch(changeAmountAction(id, amount))
  }

  function removeItem(id: string) {
    dispatch(removeItemAction(id))
  }

  function addCheckoutFormData(data: CheckoutFormData) {
    dispatch(addCheckoutFormDataAction(data))
  }

  return (
    <ShoppingCartContext.Provider
      value={{
        items,
        checkoutFormData,
        addNewItem,
        changeAmount,
        removeItem,
        addCheckoutFormData,
      }}
    >
      {children}
    </ShoppingCartContext.Provider>
  )
}
